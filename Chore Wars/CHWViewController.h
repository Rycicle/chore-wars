//
//  CHWViewController.h
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "CHWBaseViewController.h"
#import "CHWButton.h"
#import "CHWTextField.h"

@interface CHWViewController : CHWBaseViewController

@end
