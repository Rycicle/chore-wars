//
//  CHWDataManager.m
//  Chore Wars
//
//  Created by Ryan Salton on 01/07/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWDataManager.h"

@implementation CHWDataManager

+ (CHWDataManager *)sharedInstance
{
    static dispatch_once_t pred;
    static CHWDataManager *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        if(!self.userModel){
            self.userModel = [CHWUserModel new];
        }
        [self loadUserModel];
    }
    
    return self;
}

-(void)saveUserModel
{
    NSDictionary *userDict = [NSDictionary dictionaryWithObjectsAndKeys:self.userModel.householdID, @"Household", self.userModel.characterID, @"Character", nil];
//    NSDictionary *userDict = @{};
    [[NSUserDefaults standardUserDefaults] setObject:userDict forKey:@"UserDefaults"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)loadUserModel
{
    NSDictionary *userModelDictionary = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserDefaults"];
    self.userModel.householdID = [userModelDictionary objectForKey:@"Household"];
    self.userModel.characterID = [userModelDictionary objectForKey:@"Character"];
}

@end
