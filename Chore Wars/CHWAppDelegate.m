//
//  CHWAppDelegate.m
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWAppDelegate.h"
#import <Parse/Parse.h>

@implementation CHWAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [Parse setApplicationId:@"reagVoT9permZqARyaCXCjkRwFlZD4mm1BRA3GMF"
                  clientKey:@"PX0NLDXd5wh3zrC6tlgjCl6F9BXp7Zwvd8jLtWRY"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [CHWDataManager sharedInstance];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[CHWDataManager sharedInstance] saveUserModel];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[CHWDataManager sharedInstance] loadUserModel];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[CHWDataManager sharedInstance] loadUserModel];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[CHWDataManager sharedInstance] saveUserModel];
}

@end
