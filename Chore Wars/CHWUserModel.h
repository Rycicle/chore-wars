//
//  CHWUserModel.h
//  Chore Wars
//
//  Created by Ryan Salton on 01/07/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CHWUserModel : NSObject

@property (nonatomic, strong) NSString *householdID;
@property (nonatomic, strong) NSString *characterID;

@end
