//
//  CHWChoresTableViewController.h
//  Chore Wars
//
//  Created by Ryan Salton on 01/07/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface CHWChoresTableViewController : UITableViewController

@end
