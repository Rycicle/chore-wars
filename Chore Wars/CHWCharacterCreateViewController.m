//
//  CHWCharacterCreateViewController.m
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWCharacterCreateViewController.h"
#import "CHWTextField.h"
#import "CHWButton.h"
#import "CHWChoresTableViewController.h"

@interface CHWCharacterCreateViewController ()

@end

@implementation CHWCharacterCreateViewController {
    CHWTextField *charNameField;
    UIButton *unFocusTextFieldButton;
    CHWButton *savePlayerButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Character";
    
    unFocusTextFieldButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    unFocusTextFieldButton.backgroundColor = [UIColor clearColor];
    [unFocusTextFieldButton addTarget:self action:@selector(unFocusTextField) forControlEvents:UIControlEventTouchUpInside];
    unFocusTextFieldButton.hidden = YES;
    [self.view addSubview:unFocusTextFieldButton];
    
    charNameField = [[CHWTextField alloc] initWithFrame:CGRectMake(20, 88, self.view.frame.size.width - 40, 40)];
    charNameField.placeholder = @"Name";
    charNameField.textAlignment = NSTextAlignmentCenter;
    charNameField.delegate = self;
    [self.view addSubview:charNameField];
    
    
    
    savePlayerButton = [[CHWButton alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height - 60, self.view.frame.size.width - 40, 40)];
    [savePlayerButton setTitle:@"Create Character" forState:UIControlStateNormal];
    [savePlayerButton addTarget:self action:@selector(createCharacter) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:savePlayerButton];
    
    
}

-(void)createCharacter
{
    if(charNameField.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a character name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    PFObject *newChar = [PFObject objectWithClassName:@"Characters"];
    newChar[@"name"] = charNameField.text;
    newChar[@"householdId"] = [CHWDataManager sharedInstance].userModel.householdID;
    [newChar saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            PFQuery *query = [PFQuery queryWithClassName:@"Characters"];
            [query whereKey:@"name" equalTo:charNameField.text];
            [query orderByDescending:@"createdAt"];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    // Do something with the found objects
                    [CHWDataManager sharedInstance].userModel.characterID = ((PFObject*)objects[0]).objectId;
                    [[CHWDataManager sharedInstance] saveUserModel];
                    NSLog(@"Character ID: %@", [CHWDataManager sharedInstance].userModel.characterID);
                    
                    [self goToTaskList];
                    
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        }
    }];
}

-(void)goToTaskList
{
    [self.navigationController pushViewController:[CHWChoresTableViewController new] animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)unFocusTextField
{
    [charNameField resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    unFocusTextFieldButton.hidden = NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    unFocusTextFieldButton.hidden = YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
