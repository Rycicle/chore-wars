//
//  CHWButton.m
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWButton.h"

@implementation CHWButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor lightGrayColor];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
