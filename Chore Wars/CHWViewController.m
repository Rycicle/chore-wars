//
//  CHWViewController.m
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWViewController.h"
#import "CHWCharacterCreateViewController.h"
#import "CHWChoresTableViewController.h"

@interface CHWViewController ()

@end

@implementation CHWViewController {
    CHWTextField *newHouseField;
    CHWTextField *joinHouseField;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self customiseNav];
    
    // Add title image/text
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, self.navigationController.navigationBar.frame.size.height + 20, self.view.frame.size.width - 40, 120)];
    titleLabel.text = @"Chore\nWars";
    titleLabel.font = [UIFont fontWithName:@"Chalkduster" size:40];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:titleLabel];
    
    // New household
    newHouseField = [[CHWTextField alloc] initWithFrame:CGRectMake(20, titleLabel.frame.origin.y + titleLabel.frame.size.height + 20, self.view.frame.size.width - 40 - 78, 40)];
    newHouseField.placeholder = @"House name";
    [self.view addSubview:newHouseField];
    
    CHWButton *newHouseButton = [[CHWButton alloc] initWithFrame:CGRectMake(newHouseField.frame.size.width + newHouseField.frame.origin.x + 8, newHouseField.frame.origin.y, 70, 40)];
    [newHouseButton setTitle:@"Create" forState:UIControlStateNormal];
    [newHouseButton addTarget:self action:@selector(registerNewHouse) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:newHouseButton];
    
    // Join house
    CHWButton *joinHouseButton = [[CHWButton alloc] initWithFrame:CGRectMake(newHouseButton.frame.origin.x, newHouseButton.frame.origin.y + newHouseButton.frame.size.height + 20, 70, 40)];
    [joinHouseButton setTitle:@"Join" forState:UIControlStateNormal];
    [joinHouseButton addTarget:self action:@selector(joinExistingHouse) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:joinHouseButton];
    
    joinHouseField = [[CHWTextField alloc] initWithFrame:CGRectMake(newHouseField.frame.origin.x, newHouseButton.frame.origin.y + newHouseButton.frame.size.height + 20, newHouseField.frame.size.width, 40)];
    joinHouseField.placeholder = @"Household ID";
    [self.view addSubview:joinHouseField];
    
    if([CHWDataManager sharedInstance].userModel.householdID){
        if([CHWDataManager sharedInstance].userModel.characterID){
            
            [self.navigationController setViewControllers:@[[CHWChoresTableViewController new]] animated:NO];

        }
        else{
            [self goToCharacterSelectAnimated:NO];
        }
    }

}

-(void)registerNewHouse
{
    if(newHouseField.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a household name" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    PFObject *newHouse = [PFObject objectWithClassName:@"Households"];
    newHouse[@"name"] = newHouseField.text;
    [newHouse saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded){
            PFQuery *query = [PFQuery queryWithClassName:@"Households"];
            [query whereKey:@"name" equalTo:newHouseField.text];
            [query orderByDescending:@"createdAt"];
            [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    // The find succeeded.
                    // Do something with the found objects
                    [CHWDataManager sharedInstance].userModel.householdID = ((PFObject*)objects[0]).objectId;
                    [[CHWDataManager sharedInstance] saveUserModel];
                    NSLog(@"Household ID: %@", [CHWDataManager sharedInstance].userModel.householdID);
                    
                    [self goToCharacterSelectAnimated:YES];
                } else {
                    // Log details of the failure
                    NSLog(@"Error: %@ %@", error, [error userInfo]);
                }
            }];
        }
    }];
}

-(void)joinExistingHouse
{
    if(joinHouseField.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter an ID" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    PFQuery *query = [PFQuery queryWithClassName:@"Households"];
    [query getObjectInBackgroundWithId:joinHouseField.text block:^(PFObject *household, NSError *error) {
        if(!error){
            NSLog(@"%@", household);
            [CHWDataManager sharedInstance].userModel.householdID = joinHouseField.text;
            [[CHWDataManager sharedInstance] saveUserModel];
            [self goToCharacterSelectAnimated:YES];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"No households were found with this ID" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }];
    
}

-(void)goToCharacterSelectAnimated:(BOOL)animate
{
    [self.navigationController pushViewController:[CHWCharacterCreateViewController new] animated:animate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
