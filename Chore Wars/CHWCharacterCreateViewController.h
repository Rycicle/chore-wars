//
//  CHWCharacterCreateViewController.h
//  Chore Wars
//
//  Created by Ryan Salton on 30/06/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "CHWBaseViewController.h"
#import <Parse/Parse.h>

@interface CHWCharacterCreateViewController : CHWBaseViewController <UITextFieldDelegate>

@end
